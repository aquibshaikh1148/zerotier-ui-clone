import React from 'react'
import Image from 'next/image'
const Pricing = ({tier,features,action,color}) => {
  return (
    <div className='pricing-div'> 
        <Image className='my-2' src={color} alt="Img" width={64} height={64}/>
        <h3>{tier}</h3>
        <p>{features[0]}</p>
        <div>
            <p>{features[1]}</p>    
            <p>{features[2]}</p>
            <p>{features[3]}</p>
            <p>{features[4]}</p>
            <p>{features[5]}</p>
            <p>{features[6]}</p>
            
        </div>
        <button className={action==="Sign Up" ? "signup-button" : action==="Free Sign Up" ? "freeSignUp-btn" : "contact-sales"}>{action}</button>
   

   </div>
  )
}

export default Pricing