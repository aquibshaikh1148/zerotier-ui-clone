import React from 'react'
import Image from 'next/image'



const Offerings = ({tier,perks,icons}) => {
  return (
    <div className='offerings-div'>

        <Image src={icons} alt="icons" width={100} height={100}/>
        <h2>{tier}</h2>
        <p>{perks[0]}</p>
        <p>{perks[1]}</p>
        <p>{perks[2]}</p>
        <p>{perks[3]}</p>
    </div>
  )
}

export default Offerings