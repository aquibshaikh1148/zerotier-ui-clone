import React from 'react';
import Image from 'next/image';
import logoOrange from '../public/images/Zero_Tier_Logo_Orange.webp';

const Navbar = () => {
	return (
		<>
			<nav className='navbar navbar-expand-lg nav-color padding-nav'>
				<div className='container-fluid '>
					<a className='navbar-brand logo' href='#'>
						<Image src={logoOrange} alt='logo' width={28} height={28} />
						ZEROTIER
					</a>
					<button
						className='navbar-toggler'
						type='button'
						data-bs-toggle='collapse'
						data-bs-target='#navbarSupportedContent'
						aria-controls='navbarSupportedContent'
						aria-expanded='false'
						aria-label='Toggle navigation'
					>
						<span className='navbar-toggler-icon ' id='colour' />
					</button>
					<div
						className='collapse navbar-collapse nav-mar '
						id='navbarSupportedContent'
					>
						<ul className='navbar-nav me-auto mb-2 mb-lg-0 '>
							<li className='nav-item'>
								<a className='nav-link nav-m' aria-current='page' href='#'>
									Features
								</a>
							</li>
							<li className='nav-item'>
								<a className='nav-link' href='#'>
									Pricing
								</a>
							</li>
							<li className='nav-item'>
								<a className='nav-link' href='#'>
									Download
								</a>
							</li>
							<li className='nav-item dropdown '>
								<a
									className='nav-link dropdown-toggle'
									href='#'
									id='navbarDropdown'
									role='button'
									data-bs-toggle='dropdown'
									aria-expanded='false'
								>
									Company
								</a>
								<ul className='dropdown-menu' aria-labelledby='navbarDropdown'>
									<li>
										<a className='dropdown-item' href='#'>
											Contact
										</a>
									</li>
									<li>
										<a className='dropdown-item' href='#'>
											About Us
										</a>
									</li>

									<li>
										<a className='dropdown-item' href='#'>
											Careers
										</a>
									</li>
									<li>
										<a className='dropdown-item' href='#'>
											Blog
										</a>
									</li>
									<li>
										<a className='dropdown-item' href='#'>
											Media Kit
										</a>
									</li>
								</ul>
							</li>
							<li className='nav-item dropdown'>
								<a
									className='nav-link dropdown-toggle'
									href='#'
									id='navbarDropdown'
									role='button'
									data-bs-toggle='dropdown'
									aria-expanded='false'
								>
									Support
								</a>
								<ul className='dropdown-menu' aria-labelledby='navbarDropdown'>
									<li>
										<a className='dropdown-item' href='#'>
											Documentation
										</a>
									</li>
									<li>
										<a className='dropdown-item' href='#'>
											Knowledge Base
										</a>
									</li>

									<li>
										<a className='dropdown-item' href='#'>
											Community
										</a>
									</li>
									<li>
										<a className='dropdown-item' href='#'>
											Getting Started
										</a>
									</li>
								</ul>
							</li>
						</ul>
						<form className='d-flex'>
							<button className='login-button mx-2'>Login</button>
							<button className='signup-button '>Sign Up</button>
						</form>
					</div>
				</div>
			</nav>
		</>
	);
};

export default Navbar;
