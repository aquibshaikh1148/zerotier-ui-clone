import React from 'react';
import '@fortawesome/fontawesome-svg-core/styles.css'; 
import { config } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import '../fontawesome';

const Footer = () => {
	return (
		<>
			<div className='container'>
				<div className='row m-0	'>
					<div className='col-md-6'>
						<h4>ZEROTIER</h4>
						<p className='mt-3'>Securely Connecting The World&apos;s Devices.</p>
					</div>
					<div className='col-md-2 '>
						<h5>GET STARTED</h5>
						<ul className='text-start list'>
							<li className='nav-item'>
								<a className='nav-link nav-m' aria-current='page' href='#'>
									Download
								</a>
							</li>
							<li className='nav-item'>
								<a className='nav-link nav-m' aria-current='page' href='#'>
									GitHub
								</a>
							</li>
							<li className='nav-item'>
								<a className='nav-link nav-m' aria-current='page' href='#'>
									SDK
								</a>
							</li>
							<li className='nav-item'>
								<a className='nav-link nav-m' aria-current='page' href='#'>
									Partners
								</a>
							</li>
						</ul>
					</div>
					<div className='col-md-2'>
						<h5>SUPPORT</h5>
						<ul className='text-start list'>
							<li className='nav-item'>
								<a className='nav-link nav-m' aria-current='page' href='#'>
									Documentation
								</a>
							</li>
							<li className='nav-item'>
								<a className='nav-link nav-m' aria-current='page' href='#'>
									Knowledge Base
								</a>
							</li>
							<li className='nav-item'>
								<a className='nav-link nav-m' aria-current='page' href='#'>
									Community
								</a>
							</li>
							<li className='nav-item'>
								<a className='nav-link nav-m' aria-current='page' href='#'>
									Getting Started
								</a>
							</li>
						</ul>
					</div>
					<div className='col-md-2'>
						<h5>COMPANY</h5>
						<ul className='text-start list'>
							<li className='nav-item'>
								<a className='nav-link nav-m' aria-current='page' href='#'>
									Contact
								</a>
							</li>
							<li className='nav-item'>
								<a className='nav-link nav-m' aria-current='page' href='#'>
									About Us
								</a>
							</li>
							<li className='nav-item'>
								<a className='nav-link nav-m' aria-current='page' href='#'>
									Careers
								</a>
							</li>
							<li className='nav-item'>
								<a className='nav-link nav-m' aria-current='page' href='#'>
									Blog
								</a>
							</li>
							<li className='nav-item'>
								<a className='nav-link nav-m' aria-current='page' href='#'>
									Media Kit
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div className='container my-2' id='ic ons-footer'>
	  <FontAwesomeIcon icon={['fab', 'apple']} id="apple" />
      <FontAwesomeIcon icon={['fab', 'twitter']} id="twitter" />
      <FontAwesomeIcon icon={['fab', 'github']} id="github" />
      <FontAwesomeIcon icon={['fab', 'facebook']} id="facebook" />
			</div>
			<br />
				<div className='d-flex justify-content-between px-3 py-5'>
					<p>© 2023 ZeroTier, Inc. All rights reserved.</p>
					<div className='d-flex'>
						<p className='mx-4 footer-link'>
							<a className='nav-link ' aria-current='page' href='#'>
								Terms
							</a>
						</p>
						<p className='footer-link'>
							<a className='nav-link nav-m' aria-current='page' href='#'>
								Privacy Policy
							</a>
						</p>
					</div>
				</div>
			</div>
		</>
	);
};

export default Footer;
