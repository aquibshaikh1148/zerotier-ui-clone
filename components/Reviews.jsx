import React from 'react';
import Image from 'next/image';
import userIcon from '../public/images/userIcon.webp';

const Reviews = ({ message, author }) => {
	return (
		<>
			<div className=' review-div'>
				<div className='first-div'>
					<Image src={userIcon} alt='userIcon' width={50} height={50} />
				</div>
				<div className=' second-div'>
					<p>{message}</p>
					<p>{author}</p>
				</div>
			</div>
		</>
	);
};

export default Reviews;
