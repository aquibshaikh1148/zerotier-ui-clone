import React from 'react';
import Image from 'next/image';
import cardImg1 from '../public/images/cardImg1.webp';
import cardImg2 from '../public/images/cardImg2.webp';
import cardImg3 from '../public/images/Continuous_Integration.webp';
import cardImg4 from '../public/images/Sales_Graphs_Art.webp';
import logo from '../public/images/Zero_Tier_Logo.webp';
import logoOrange from '../public/images/Zero_Tier_Logo_Orange.webp';
import logoBlue from '../public/images/Zero_Tier_Logo_Blue.webp';
import game_icon from '../public/images/game_icon.webp';
import cloud_icon from '../public/images/Cloud_Icon.webp';
import global_icon from '../public/images/Global_Network_Icon.webp';
import devOps_icon from '../public/images/Dev_Ops_Icon.webp';
import Reviews from './Reviews';
import Offerings from './Offerings';
import Pricing from './Pricing';

const PageContent = () => {
	let Data = {
		reviews: [
			{
				message:
					'Metropolis deploys computer vision in parking lots & car wash facilities across the US, integrating ZeroTier to eliminate the complexity of managing multiple, disparate networks to deliver a seamless, groundbreaking and world-class customer experience.',
				author: 'TODD SHIPWAY, HEAD OF TECHNICAL OPERATIONS, METROPOLIS',
			},
			{
				message:
					'In early product development, we needed a way to easily connect our growing IoT product-base to our systems. ZeroTier provided an easy, and reliable way to achieve that, and has been growing with us.',
				author: 'PETER BOIN, PRINCIPAL SOFTWARE ENGINEER, ALLUME ENERGY',
			},
			{
				message:
					'ZeroTier provides a robust and essential backbone for our communications stack.',
				author: 'ANDREW LIPSCOMB, MECHATRONICS ENGINEER, SWARMFARM ROBOTICS',
			},
			{
				message:
					'Loft Orbital uses ZeroTier to improve interoperability between its offices in the United States and France. It connects our engineers to key resources quickly and easily, which allows our team to focus on making space simple.',
				author: 'BRUNSTON POON, SOFTWARE ENGINEER, LOFT ORBITAL',
			},
		],
		offerings: [
			{
				tier: 'Individuals',
				perks: [
					'Access your computers, NAS, home automation, IP cameras, ham radios or other devices from anywhere',
					'Conveniently share files and data, or even play LAN games with others',
					'Manage secure network access to users of choice',
				],
				icons: game_icon,
			},
			{
				tier: 'IT Teams',
				perks: [
					'Simplify your network stack by unifying VPNs, VLANs, and SD-WANs with one solution',
					'Build, manage, and observe any number of remote, on premise, or cloud networks with one management interface',
					'Easily provision remote access for all of your users',
				],
				icons: cloud_icon,
			},
			{
				tier: 'DevOps',
				perks: [
					'Quickly build backplane networks spanning multiple cloud providers',
					'Save on performance, storage, and bandwidth',
					'Administrate and debug from anywhere',
					'Secure corporate network overlay and failover layer',
				],
				icons: devOps_icon,
			},
			{
				tier: 'Embedded',
				perks: [
					'Enjoy vastly superior network control and functionality',
					'Develop and manage products or services running on their own decentralized networks',
					'Create 4G/5G-capable secure networks for any IoT, edge or embedded device that can operate on 64MB of RAM',
				],
				icons: global_icon,
			},
		],
		pricing: [
			{
				tier: 'Basic',
				features: [
					'For Everyone / ZeroTier Hosted Controller',
					'✓ 1 Admin',
					'✓ 25 Nodes',
					'✓ Unlimited Networks',
					'✖ Business SSO: n/a',
					'✓ Community Support',
					'FREE',
				],
				action: 'Free Sign Up',
				color: logo,
			},
			{
				tier: 'Professional',
				features: [
					'Licensed Only For Individuals and Testing',
					'✓ Admins | $10 USD/mo each',
					'✓ 25 Node Packs | $5 USD/mo',
					'✓ Unlimited Networks',
					'✓ Business SSO | $5 USD/mo per seat',
					'✓ Community Support',
					'Starting at $5 USD/month',
				],
				action: 'Sign Up',
				color: logoOrange,
			},
			{
				tier: 'Business',
				features: [
					'Licensed for Commercial Deployments',
					'Use Cases Include:',
					'∙ IoT/IIoT',
					'∙ SD-WAN',
					'∙ VPN',
					'∙ Remote Monitoring and Management',
					'Contact Sales for Pricing',
				],
				action: ' Contact Sales',
				color: logoBlue,
			},
		],
	};

	return (
		<>
			<div className='container col-xxl-8 px-4 '>
				<div className='row flex-lg-row-reverse align-items-center g-5 pt-5'>
					<div className='col-10 col-sm-8 col-lg-6'></div>
					<div className='col-lg-6'>
						<h1 className='display-5 fw-bold text-light lh-1 mb-3'>
							Securely connect any device, anywhere.
						</h1>
						<p className='fs-5  text-light'>
							Simple and affordable price plans for you and your dog.ZeroTier
							lets you build modern, secure multi-point virtualized networks of
							almost any type. From robust peer-to-peer networking to
							multi-cloud mesh infrastructure, we enable global connectivity
							with the simplicity of a local network.
						</p>
						<div className='d-grid gap-2 d-md-flex justify-content-md-start'>
							<button type='button' className='signup-button'>
								Get ZeroTier
							</button>
							<button type='button' className='login-button'>
								Learn MORE &#11166;
							</button>
						</div>
					</div>
				</div>
			</div>
			<div className='my-5'>
				<h5 className=' text-center  color '>
					🔔 NEW!&nbsp;
					<a className='link' href=''>
						ZeroTier Webhooks
					</a>
					- Current Release:&nbsp;
					<a className='link' href=''>
						Download ZeroTier 1.12.2
					</a>
				</h5>
			</div>
			{/* card section */}
			<div className='card-div '>
				<div className='card mb-3 card-bgColor' style={{ maxWidth: '80%' }}>
					<div className='row g-0'>
						<div className='col-md-6'>
							<Image
								style={{ width: '90%', height: '90%' }}
								src={cardImg1}
								alt='picture'
							/>
						</div>
						<div className='col-md-6'>
							<div className='card-body card-div2'>
								<h3 className='card-title'>It just works</h3>
								<p className='card-text'>
									ZeroTier combines the capabilities of VPN and SD-WAN,
									simplifying network management. Enjoy flexibility while
									avoiding costly hardware vendor lock in.
								</p>
								<div>
									<button className='freeSignUp-btn '>
										Learn more &#11166;
									</button>
								</div>
							</div>
						</div>
					</div>
					{/*  */}
					<div className='row g-0'>
						<div className='col-md-6'>
							<div className='card-body card-div2'>
								<h3 className='card-title'>Speed, flexibility, and security</h3>
								<p className='card-text'>
									Set up ZeroTier in minutes with remote, automated deployment.
								</p>
								<p>
									Emulates Layer 2 Ethernet with multipath, multicast, and
									bridging capabilities.
								</p>
								<p>
									ZeroTier’s zero-trust networking solution provides scalable
									security with 256-bit end-to-end encryption.
								</p>
								<div>
									<button className='freeSignUp-btn '>Get ZeroTier </button>
								</div>
							</div>
						</div>
						<div className='col-md-6'>
							<Image
								style={{ width: '90%', height: '90%' }}
								src={cardImg2}
								alt='picture'
							/>
						</div>
					</div>
				</div>
			</div>
			<div>
				<h1 className='text-center my-4'>
					Used by the world&apos;s most innovative teams
				</h1>
			</div>

			<div className='reviews'>
				{Data.reviews.map((e, index) => (
					<Reviews key={index} message={e.message} author={e.author} />
				))}
			</div>

			<div>
				<h1 className='text-center my-4'>
					Secure networks for teams of any size
				</h1>

				<div className='offerings'>
					{Data.offerings.map((e, index) => (
						<Offerings
							key={index}
							tier={e.tier}
							perks={e.perks}
							icons={e.icons}
						/>
					))}
				</div>
			</div>

			<div>
				<h1 className='text-center my-4'>Pricing</h1>
				<p className='text-center'>
					ZeroTier makes networking easy for everyone - anywhere.
				</p>
				<div className='pricing'>
					{Data.pricing.map((e, index) => (
						<Pricing
							key={index}
							tier={e.tier}
							features={e.features}
							action={e.action}
							color={e.color}
						/>
					))}
				</div>
			</div>

			<div className='card-div1'>
				<div className='card mb-3 card-bgColor1' style={{ maxWidth: '80%' }}>
					<div className='row g-0 '>
						<div className='col-md-8 '>
							<div className='card-body card-div2'>
								<h3 className='card-title'>
									<strong>ZeroTier Enterprise</strong>
								</h3>
								<p className='card-text'>
									For high-volume VPN, IoT, IIoT, edge, embedded networking,
									multi & hybrid cloud, Infrastructure as Code (IaC), and
									Commercial Use.
								</p>
								<div>
									<button className='contactSales-btn '>Contact Sales</button>
								</div>
							</div>
						</div>
						<div className='col-md-4'>
							<Image
								style={{ width: '90%', height: '90%' }}
								src={cardImg3}
								alt='picture'
							/>
						</div>
					</div>
				</div>
				<div
					className='card mb-3 card-bgColor1 my-5'
					style={{ maxWidth: '80%' }}
				>
					<div className='row g-0'>
						<div className='col-md-4'>
							<Image
								style={{ width: '90%', height: '90%' }}
								src={cardImg4}
								alt='picture'
							/>
						</div>
						<div className='col-md-8'>
							<div className='card-body card-div2'>
								<h3 className='card-title'>
									<strong>Service Providers</strong>
								</h3>
								<p className='card-text'>
									For hardware and software companies interested in embedding or
									integrating the ZeroTier platform within their service
									offering, as well as enterprise-facing channels including
									Managed Service Providers (MSPs), Resellers, and Systems
									Integrators.
								</p>
								<div>
									<button className='contactSales-btn'>Learn more</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div className='container opensource-div my-5 py-5'>
				<div>
					<h3>Open Source Community Edition</h3>
					<p>
						Featuring unlimited nodes, networks, and admins. Self-hosted.
						Designed for non-Commercial Use cases.
					</p>
					<form className='d-flex '>
						<button className='zerotier-btn mx-2'>ZeroTier GitHub</button>
						<button className='read-btn '>Read Documentation</button>
					</form>
				</div>
			</div>

			<div className='container subscribe-div my-5'>
				<div className='  row m-0'>
					<div className='col-md-8'>
						<h4 className='pt-4'>Newsletter Signup</h4>
						<p className='pb-3'>
							Join over 350,000 community members worldwide and receive the
							latest news from Team ZeroTier.
						</p>
					</div>
					<div className='col-md-4'>
						<form className='d-flex'>
							<input
								className='form-control  mt-5 mx-3 mb-2'
								type='search'
								placeholder='Email Address'
								aria-label='Search'
								width='200px'
							/>
							<button className='btn subscribe-btn mt-5 mb-2' type='submit'>
								subscribe
							</button>
						</form>
					</div>
				</div>
			</div>
		</>
	);
};

export default PageContent;
