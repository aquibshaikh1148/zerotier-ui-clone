"use client"
import Image from 'next/image'
import 'bootstrap/dist/css/bootstrap.css'
import './globals.css'
import { useEffect } from 'react';
import Navbar from "../../components/Navbar"
import Footer from "../../components/Footer"
import PageContent from "../../components/PageContent"


export default function Home() {
  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js");
  }, []);
  return (
 <>
       
 <div className='main-div'>
 <Navbar/>
 <PageContent/>
 <Footer/>

 </div>


 </>
  )
}
