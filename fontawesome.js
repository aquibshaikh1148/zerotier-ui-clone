import { library } from '@fortawesome/fontawesome-svg-core';
import { faApple, faTwitter, faGithub, faFacebook } from '@fortawesome/free-brands-svg-icons';

library.add(faApple, faTwitter, faGithub, faFacebook);